using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Sentry;
using Sentry.Unity;
using Sentry.WebGL;
using UnityEngine;
using UnityEngine.InputSystem;

public class TestCase : MonoBehaviour
{
    [SerializeField] private InputAction bKeyAction;
    [SerializeField] private InputAction aKeyAction;

    private SentryUnityOptions sentryOptions;

    // Start is called before the first frame update
    void Start()
    {
        aKeyAction.Enable();
        bKeyAction.Enable();

        sentryOptions = new SentryUnityOptions
        {
            Dsn = "REMOVED",
            ReportAssembliesMode = ReportAssembliesMode.None, // Reduce event sizes
            AutoSessionTracking = false,
            TracesSampleRate = 0, // No session update
            CaptureInEditor = true,
            Debug = true,
            DebugOnlyInEditor = false,
            DiagnosticLevel = SentryLevel.Debug
        };
        // WebGL doesn't currently have a working standard background worker, but we can replace it
        // to send over normal UnityWebRequest
        sentryOptions.BackgroundWorker = new WebGLEnvelopeSender(sentryOptions.Dsn);
        InvokeRepeating("FlushSentryErrors", 1, 1);
        SentryUnity.Init(sentryOptions);
    }

    private async void FlushSentryErrors()
    {
        await sentryOptions.BackgroundWorker.FlushAsync(TimeSpan.Zero);
    }

    // Update is called once per frame
    void Update()
    {
        if (aKeyAction.triggered)
        {
            Debug.Log("'A' key triggered");
            StartCoroutine(SendErrorReportBroken());
        }
        if (bKeyAction.triggered)
        {
            Debug.Log("'B' key triggered");
            StartCoroutine(SendErrorReportWorking());
        }
    }

    private string CaptureScreenshot()
    {
#if UNITY_EDITOR
        var filePath = $"Screenshot-{DateTime.UtcNow:yyyy-MM-ddTHH-mm-ss}.jpg";
        filePath = Path.Combine(Application.dataPath, "../", filePath);
#else
        var filePath = $"{Application.persistentDataPath}/Screenshot-{DateTime.UtcNow:yyyy-MM-ddTHH-mm-ss}.png";
#endif
        ScreenCapture.CaptureScreenshot(filePath);
        return filePath;
    }

    private IEnumerator SendErrorReportBroken()
    {
        var filePath = CaptureScreenshot();

        // Wait for screenshot to be saved
        if (!File.Exists(filePath))
        {
            yield return null;
        }
        // Waits for up to 1s to check that the file exists
        var eventId = SentrySdk.CaptureMessage("Broken error report", scope =>
        {
            scope.AddAttachment(filePath);
        });
        SentrySdk.CaptureUserFeedback(eventId, "test@email.com", "error text", "Test name");
    }

    private IEnumerator SendErrorReportWorking()
    {
        var filePath = CaptureScreenshot();

        // Wait for screenshot to be saved
        if (!File.Exists(filePath))
        {
            yield return null;
        }
        // Waits for up to 1s to check that the file exists
        var eventId = SentrySdk.CaptureMessage("Working error report", scope =>
        {
            using Stream fileStream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            var data = new byte[fileStream.Length];
            fileStream.Read(data, 0, data.Length);
            fileStream.Seek(0, SeekOrigin.Begin);
            scope.AddAttachment(data, Path.GetFileName(filePath));
        });
        SentrySdk.CaptureUserFeedback(eventId, "test@email.com", "error text", "Test name");
    }
}
