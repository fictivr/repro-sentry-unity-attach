using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Sentry;
using Sentry.Extensibility;
using UnityEngine;
using UnityEngine.Networking;

namespace Sentry.WebGL
{

    internal class WebGLEnvelopeSender : IBackgroundWorker
    {
        private string publicKey;
        private string secretKey;
        private Uri apiUri;
        private int requestsSent = 0;

        public WebGLEnvelopeSender(string dsn)
        {
            if (dsn == "")
            {
                throw new ArgumentException("invalid argument - DSN cannot be empty");
            }
            var _uri = new Uri(dsn);
            if (string.IsNullOrEmpty(_uri.UserInfo))
            {
                throw new ArgumentException("Invalid DSN: No public key provided.");
            }
            var keys = _uri.UserInfo.Split(':');
            publicKey = keys[0];
            if (string.IsNullOrEmpty(publicKey))
            {
                throw new ArgumentException("Invalid DSN: No public key provided.");
            }
            secretKey = null;
            if (keys.Length > 1)
            {
                secretKey = keys[1];
            }

            var path = _uri.AbsolutePath.Substring(0, _uri.AbsolutePath.LastIndexOf('/'));
            var projectId = _uri.AbsoluteUri.Substring(_uri.AbsoluteUri.LastIndexOf('/') + 1);

            if (string.IsNullOrEmpty(projectId))
            {
                throw new ArgumentException("Invalid DSN: A Project Id is required.");
            }

            var builder = new UriBuilder
            {
                Scheme = _uri.Scheme,
                Host = _uri.DnsSafeHost,
                Port = _uri.Port,
                Path = string.Format("{0}/api/{1}/envelope/", path, projectId)
            };
            apiUri = builder.Uri;
        }

        private Queue<Protocol.Envelopes.Envelope> envelopeQueue = new Queue<Protocol.Envelopes.Envelope>();
        public int QueuedItems => envelopeQueue.Count;

        public bool EnqueueEnvelope(Protocol.Envelopes.Envelope envelope)
        {
            envelopeQueue.Enqueue(envelope);
            var id = envelope.TryGetEventId();
            // Debug.Log($"Queued Sentry error {IdentifyEnvelope(envelope)}");
            return true;
        }

        private string IdentifyEnvelope(Protocol.Envelopes.Envelope envelope)
        {
            var evId = envelope.TryGetEventId();
            string firstItemType = "";
            if (envelope.Items?.Count > 0 && envelope.Items[0].Header.ContainsKey("type"))
                firstItemType = (string)envelope.Items[0].Header["type"];
            return $"Event={evId}, type={firstItemType}";
        }

        public async Task FlushAsync(TimeSpan timeout)
        {
            // Debug.Log($"Asked to flush queue with count: {envelopeQueue.Count}");
            while (envelopeQueue.Count > 0)
            {
                var env = envelopeQueue.Dequeue();
                // Debug.Log($"Dequeuing and sending envelope {IdentifyEnvelope(env)}");
                try
                {
                    await SendHTTP(env);
                }
                catch (Exception e)
                {
                    Debug.LogWarning($"Got exception in SendHTTP {e}");
                }
            }
        }

        private async Task SendHTTP(Protocol.Envelopes.Envelope env)
        {
            var timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH\\:mm\\:ss");
            var authString = string.Format("Sentry sentry_version=5,sentry_client=Unity0.1," +
                     "sentry_timestamp={0}," +
                     "sentry_key={1}," +
                     "sentry_secret={2}",
                     timestamp,
                     publicKey,
                     secretKey);
            using var ms = new MemoryStream();
            string? resp;
            try
            {
                // Debug.Log($"Will serialize {IdentifyEnvelope(env)}");
                await env.SerializeAsync(ms, null);
                // Debug.Log($"Will POST {IdentifyEnvelope(env)}");
                resp = await PostBinaryAsync(apiUri.ToString(), ms.ToArray(), new Dictionary<string, string>() { ["X-Sentry-Auth"] = authString });//, $"request-{requestsSent++}.txt");
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Got error from {apiUri}");
                resp = e.ToString();
            }
            Debug.Log($"Sentry responded {resp} to {IdentifyEnvelope(env)}");
        }

        public async Task<string> PostBinaryAsync(string url, byte[] data, Dictionary<string, string> headers = null, bool debug = false)
        {
            var www = UnityWebRequest.Get(url);
            www.method = UnityWebRequest.kHttpVerbPOST;

            if (headers != null)
            {
                foreach (KeyValuePair<string, string> entry in headers)
                {
                    www.SetRequestHeader(entry.Key, entry.Value);
                }
            }
            //www.useHttpContinue = false;
            //www.chunkedTransfer = false;
            www.uploadHandler = new UploadHandlerRaw(data);

            var asyncOp = www.SendWebRequest();
            var tcs = new TaskCompletionSource<object>();
            asyncOp.completed += obj => { tcs.SetResult(null); };
            await tcs.Task;

            if (www.isNetworkError)
            {
                // TODO Possibly retry download
                throw new Exception($"{www.error}\n{url}");
            }
            else if (www.isHttpError)
            {
                string errorText = "";
                // Calling text on downloadHandler can sometimes, for some reason, throw exception "String access not supported"
                try
                {
                    errorText = www.downloadHandler?.text ?? "";
                }
                catch (Exception) { }
                throw new Exception($"{www.error}\n{url}\n{errorText}");
            }
            return www.downloadHandler.text;
        }
    }


}